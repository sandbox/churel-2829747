<?php
/**
 * @file
 * Home of the FeedsFileFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsFileFetcher.
 */
class PictionFetcherResult extends FeedsFetcherResult {

  /**
   * The URL of the feed being fetched.
   *
   * @var string
   */
  protected $url;

  /**
   * The timeout in seconds to wait for a download.
   *
   * @var int
   */
  protected $timeout;

  /**
   *
   * Whether to ignore SSL validation errors.
   *
   * @var bool
   */
  protected $acceptInvalidCert;

  /**
   * Constructor.
   */
  public function __construct($query = NULL, $config) {
    $this->query = $query;
    $this->parser_config = $config;
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    if (!isset($this->raw)) {

      feeds_include_library('http_request.inc', 'http_request');
      //First get the SURL
      //Be sure we need a new token
      $connexion_timestamp = variable_get('piction_authorization_connexion_timestamp', 0);
      if(time() > $connexion_timestamp ) {
	      $surl_parameter  = 'n=Piction_login&USERNAME=' . $this->parser_config['piction_username'] . '&PASSWORD='. $this->parser_config['piction_pass'];
	      $surl_url = $this->parser_config['auto_scheme'] . $this->parser_config['piction_url'] . '/piction/!soap.ajaxget?' . $surl_parameter;
	      $result = http_request_get($surl_url, NULL, NULL, $this->acceptInvalidCert, $this->timeout);
	      if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
	        throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
	      }
	      $json = json_decode($result->data);
	      $surl = $json['SURL'];
	      $connexion_timestamp += $this->parser_config['connexion_time'];
	      variable_set('piction_authorization_connexion_timestamp', $connexion_timestamp);
	      variable_set('piction_authorization_surl', $surl);
      }
      else {
      	$surl = variable_set('piction_authorization_surl', null);
      }

      $url = $this->parser_config['auto_scheme'] . $this->parser_config['piction_url'] . '/' . $this->query . '&SURL=' . $surl;
      $result = http_request_get($surl_url, NULL, NULL, $this->acceptInvalidCert, $this->timeout);
	  if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
	    throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
	  }
      $this->raw = $result->data;
    }

    return $this->sanitizeRaw($this->raw);
  }

  public function getTimeout() {
    return $this->timeout;
  }

  public function setTimeout($timeout) {
    $this->timeout = $timeout;
  }

  /**
   * Sets the accept invalid certificates option.
   *
   * @param bool $accept_invalid_cert
   *   Whether to accept invalid certificates.
   */
  public function setAcceptInvalidCert($accept_invalid_cert) {
    $this->acceptInvalidCert = (bool) $accept_invalid_cert;
  }

}

class PictionFetcher extends FeedsHTTPFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $fetcher_result = new PictionFetcherResult($source_config['source'], $this->config);
    // When request_timeout is empty, the global value is used.
    $fetcher_result->setTimeout($this->config['request_timeout']);
    $fetcher_result->setAcceptInvalidCert($this->config['accept_invalid_cert']);
    return $fetcher_result;
  }

  /**
   * Clear caches.
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = $source_config['source'];
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Implements FeedsFetcher::request().
   */
  public function request($feed_nid = 0) {
    feeds_dbg($_GET);
    @feeds_dbg(file_get_contents('php://input'));
    // A subscription verification has been sent, verify.
    if (isset($_GET['hub_challenge'])) {
      $this->subscriber($feed_nid)->verifyRequest();
    }
    // No subscription notification has ben sent, we are being notified.
    else {
      try {
        feeds_source($this->id, $feed_nid)->existing()->import();
      }
      catch (Exception $e) {
        // In case of an error, respond with a 503 Service (temporary) unavailable.
        header('HTTP/1.1 503 "Not Found"', NULL, 503);
        drupal_exit();
      }
    }
    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', NULL, 200);
    drupal_exit();
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'piction_url' => '',
      'piction_username' => '',
      'piction_pass' => '',
      'designated_hub' => '',
      'request_timeout' => NULL,
      'auto_scheme' => 'http',
      'accept_invalid_cert' => FALSE,
      'connexion_time' => 3600
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
    //Base URL of Piction API
    $form['piction_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Piction Base Url (without protocol)'),
      '#description' => t('Base URL of the Piction database'),
      '#default_value' => $this->config['piction_url'],
      '#size'=> 30,
      '#required' => true
    );
     //username for Piction API
    $form['piction_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username for Piction API'),
      '#description' => t('Your username for Piction API'),
      '#default_value' => $this->config['piction_username'],
      '#size'=> 30,
      '#required' => true
    );
    //Password for Piction API
    $form['piction_pass'] = array(
      '#type' => 'password',
      '#title' => t('Password for Piction API'),
      '#description' => t('Your password for Piction API'),
      '#default_value' => $this->config['piction_pass'],
      '#size'=> 30,
      '#required' => true
    );
    // Per importer override of global http request timeout setting.
    $form['request_timeout'] = array(
      '#type' => 'textfield',
      '#title' => t('Request timeout'),
      '#description' => t('Timeout in seconds to wait for an HTTP get request to finish.</br>' .
                         '<b>Note:</b> this setting will override the global setting.</br>' .
                         'When left empty, the global value is used.'),
      '#default_value' => $this->config['request_timeout'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#maxlength' => 3,
      '#size'=> 30,
    );
    $form['accept_invalid_cert'] = array(
      '#type' => 'checkbox',
      '#title' => t('Accept invalid SSL certificates'),
      '#description' => t('<strong>IMPORTANT:</strong> This setting will force cURL to completely ignore all SSL errors. This is a <strong>major security risk</strong> and should only be used during development.'),
      '#default_value' => $this->config['accept_invalid_cert'],
    );

    $form['connexion_time'] = array(
      '#type' => 'textbox',
      '#title' => t('Connexion time on Piction API'),
      '#description' => t('Time during what we use the same token to get data from Piction API'),
      '#element_validate' => array('element_validate_integer_positive'),
      '#default_value' => $this->config['connexion_time'],
    );
    return $form;
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Query'),
      '#description' => t('The query you want to do on the Piction API'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   * TODO, No validation for now
   */
  public function sourceFormValidate(&$values) {
   
  }

  /**
   * Override sourceSave() - subscribe to hub.
   */
  public function sourceSave(FeedsSource $source) {
  }

  /**
   * Override sourceDelete() - unsubscribe from hub.
   */
  public function sourceDelete(FeedsSource $source) {
  
  }


}
